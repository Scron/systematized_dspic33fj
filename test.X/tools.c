#include <xc.h>

/* convert BCD to DEC */
unsigned char utilBcd2Dec(unsigned char bcd)
{
    unsigned char decimal;

    decimal = (bcd & 0xF) + ((bcd & 0xF0) >> 4)*10;
    return decimal;
}

/* convert DEC to BCD */
unsigned char utilDec2Bcd(unsigned char decimal)
{
    unsigned char bcd;

    bcd = ((decimal/10) << 4) + (decimal % 10);
    return bcd;
}

unsigned char utilBcd2Hex(unsigned char bcd)
{
    unsigned char decimal;
    decimal = (bcd & 0xF) + ((bcd & 0xF0) >> 4)*10;
    return decimal;
}

/* FIXME: to be verified */
unsigned char utilHex2Bcd(unsigned char hex)
{
    unsigned char bcd;
    bcd = ((hex/10) << 4) + (hex % 10);
    return bcd;
}

