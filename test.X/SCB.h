#ifndef SCB_H
#define	SCB_H

const unsigned char SCB_addr_MMDD = 0x1000;
const unsigned char SCB_addr_hhmm = 0x1001;
const unsigned char SCB_addr_YYss = 0x1002;


typedef struct{
    unsigned char year;
    unsigned char month;
    unsigned char date;
    unsigned char hour;
    unsigned char min;
    unsigned char sec;
}SCB_time_t;

#endif	/* SCB_H */

