#include <xc.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "uart.h"
#include "modbus.h"
#include "time.h"
#include "crc.h"

NormalArray_t unexpectedData = {
                    .buf = {0},
                    .counter = 0
              };

static UartFuncStructure MB_FuncStrict;
static uint8_t _MBstatus = MB_IDLE,predictRxcounter = 0;
static MB_struct_t MB_struct;
static int8_t _MB_buf[256] = {0};
static unsigned char timeout_timer = 0;

static inline uint8_t lowByte(uint16_t object)
{
    return (uint8_t) ((object) & 0xFF00) >> 8;
}

static inline uint8_t highByte(uint16_t object)
{
    return (uint8_t) ((object) >> 8);
}

inline uint8_t CheckModbusStatus(void)
{
    return _MBstatus;
}

void getModbusPackage(uint8_t *buf)
{
    strncpy(buf,MB_struct.buf,predictRxcounter);
    _MBstatus = MB_IDLE;
}

void MB_init(UartFuncStructure object, unsigned int baud)
{
    MB_FuncStrict = object;
    MB_FuncStrict.func_init(baud);
    timeout_timer = ReqAvailableTimer();
    if (SetTimer_us(timeout_timer, 1000) != SUCCES) {
        timeout_timer = 0;  /* If there was no available timer, 
                             * force the No.0 timer to be the timeout_timer */
        SetTimer_us(timeout_timer, 1000);
    }
}

void MB_SetRequirePackage(uint8_t SlaveID,
        uint8_t FunctionCode,
        uint16_t Address,
        uint8_t Qty,
        int8_t *buf)
{
    if (SlaveID == 0) {
        _MBstatus MB_INVALID_SLAVE_ID;
        return;
    }
    if (Qty >= MB_MAX_BUFFER_SIZE) {
        _MBstatus MB_ILLEGAL_QTY;
        return;
    }
    if (FunctionCode == MB_WRITE_SINGLE_REGISTER ||
            FunctionCode == MB_WRITE_MULTIPLE_REGISTERS) {
        if (buf == NULL) {
            _MBstatus MB_INVAIL_ARGUMENT;
            return;
        }
        strncpy(_MB_buf, buf, Qty);
        predictRxcounter = 8;
    }

    MB_struct.SlaveID = SlaveID;
    MB_struct.FunctionCode = FunctionCode;
    MB_struct.Address = Address;
    MB_struct.Qty = Qty;
    MB_struct.buf = _MB_buf;

    _MBstatus = MB_TRANSMITFUNCTION;
}

unsigned char MB_TransmitFunction(void)
{
    char ModbusADU[256];
    unsigned char ModbusADUSize = 0;
    unsigned int u16CRC = 0;
    int i = 0;

    if (MB_struct.Address == 0) {
        _MBstatus = MB_INVALID_SLAVE_ID;
    }
    if (MB_struct.buf == NULL) {
        _MBstatus = VOID_DATA_ARRAY;
    }

    ModbusADU[ModbusADUSize++] = MB_struct.SlaveID;
    ModbusADU[ModbusADUSize++] = MB_struct.FunctionCode;
    ModbusADU[ModbusADUSize++] = highByte(MB_struct.Address);
    ModbusADU[ModbusADUSize++] = lowByte(MB_struct.Address);

    switch (MB_struct.FunctionCode) {
    case MB_READ_INPUT_REGISTERS:
    case MB_READ_HOLDING_REGISTERS:
        ModbusADU[ModbusADUSize++] = highByte(MB_struct.Qty);
        ModbusADU[ModbusADUSize++] = lowByte(MB_struct.Qty);
        predictRxcounter = 5 + (MB_struct.Qty*2);
        break;

    case MB_WRITE_SINGLE_REGISTER:
        ModbusADU[ModbusADUSize++] = highByte(*MB_struct.buf);
        ModbusADU[ModbusADUSize++] = lowByte(*MB_struct.buf);
        break;

    case MB_WRITE_MULTIPLE_REGISTERS:
        ModbusADU[ModbusADUSize++] = highByte(MB_struct.Qty);
        ModbusADU[ModbusADUSize++] = lowByte(MB_struct.Qty);
        ModbusADU[ModbusADUSize++] = (MB_struct.Qty << 1);

        for (i = 0; i < MB_struct.Qty; i++) {
            ModbusADU[ModbusADUSize++] = highByte(MB_struct.buf[i]);
            ModbusADU[ModbusADUSize++] = lowByte(MB_struct.buf[i]);
        }
        break;
    }
    // append CRC
    u16CRC = 0xFFFF;
    for (i = 0; i < ModbusADUSize; i++) {
        u16CRC = crc16_update(u16CRC, ModbusADU[i]);
    }
    ModbusADU[ModbusADUSize++] = lowByte(u16CRC);
    ModbusADU[ModbusADUSize++] = highByte(u16CRC);

    // transmit request package
    MB_FuncStrict.func_writeUartMsg(ModbusADU, ModbusADUSize);
    ResetTimerCounter(timeout_timer);   /* Reset Timer for timing how long
                                         * would modbus slave spend on response */
    _MBstatus = MB_WAITING_RESPONSE;
}

void MB_WaitingResponse(void)
{
    unsigned int UARTstatus = 0;
    if (MB_FuncStrict.func_getNewdata() == 1) {
        UARTstatus = MB_FuncStrict.func_readUartBuf(MB_struct.buf,predictRxcounter);
        if(UARTstatus == UART_NOW_RECIEVING){
            return;
        }else if(UARTstatus == UART_SUCCESS){
            _MBstatus = MB_GET_PACKAGE;
//            detectUnexpectData(MB_FuncStrict,unexpectedData);
        }
    }
    if (CheckTimerCounter(timeout_timer) >= 2000) {
        _MBstatus = MB_RESPONSE_TIMEDOUT;
        return;
    }
    _MBstatus = MB_WAITING_RESPONSE;
}

void MB_task(void)
{
    switch (_MBstatus) {
    case MB_TRANSMITFUNCTION:
        MB_TransmitFunction();
        break;
    case MB_WAITING_RESPONSE:
        MB_WaitingResponse();
        if (_MBstatus == MB_RESPONSE_TIMEDOUT) {
            _MBstatus = MB_TRANSMITFUNCTION;
        }
        break;
    case MB_GET_PACKAGE:
        break;
    default:
        _MBstatus = MB_IDLE;
        break;
    }
}