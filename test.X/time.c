#include <stdio.h>
#include <string.h>
#include <xc.h>
#include "main.h"
#include "time.h"
#include "uart.h"
#include "modbus.h"

inline static void ReleaseTimer1(void);
inline static void ReleaseTimer2(void);
inline static void ReleaseTimer3(void);
inline static void ReleaseTimer4(void);
inline static void ReleaseTimer5(void);
inline static void ReleaseTimer6(void);
static void initTIMER1(unsigned int time);
static void initTIMER2(unsigned int time);
static void initTIMER3(unsigned int time);
static void initTIMER4(unsigned int time);
static void initTIMER5(unsigned int time);
static void initTIMER6(unsigned int time);

TimerIndex_def TimerArray[TIMER_NUM] = {
    {initTIMER1,ReleaseTimer1,TIMER_DISABLE,0,0},
    {initTIMER2,ReleaseTimer2,TIMER_DISABLE,0,0},
    {initTIMER3,ReleaseTimer3,TIMER_DISABLE,0,0},
    {initTIMER4,ReleaseTimer4,TIMER_DISABLE,0,0},
    {initTIMER5,ReleaseTimer5,TIMER_DISABLE,0,0},
    {initTIMER6,ReleaseTimer6,TIMER_DISABLE,0,0},
};

unsigned char ReqAvailableTimer(void)
{
    int i = 0;
    while(i < TIMER_NUM){
        if(TimerArray[i++].status == TIMER_DISABLE){
            return i-1;
        }
    }
    return NONE_AVAILABLE_TIMER;
}

unsigned char SetTimer_us(unsigned char object,unsigned int time_us)
{
    if(object >= TIMER_NUM || time_us > TIMER_UPPER_LIMIT){
        return INVAID_ARGUMENT;
    }
    TimerArray[object].TimerFunc_ptr(time_us);
    TimerArray[object].status = TIMER_ENABLE;
    return SUCCES;
}

unsigned char ReleaseTimer(unsigned char object)
{
    if(object >= TIMER_NUM){
        return INVAID_ARGUMENT;
    }
    TimerArray[object].ReleaseTimer_ptr();
    return SUCCES;
}

inline unsigned long CheckTimerCounter(unsigned char object)
{
    return TimerArray[object].TimerCounter;
}

inline void ResetTimerCounter(unsigned char object)
{
    TimerArray[object].TimerCounter = 0;
}

static void initTIMER1(unsigned int time)
{
    PR1 = 5*time;
    T1CON = 0x0000;
    T1CONbits.TCKPS = TIMER_PRESCALE_1_8;    
    T1CONbits.TON = 1;      /* enable timer */
    IFS0bits.T1IF = 0;
    IEC0bits.T1IE = 1;
}

inline static void ReleaseTimer1(void)
{
    T1CONbits.TON = 0;
    TimerArray[0].status = TIMER_DISABLE;
    TimerArray[0].TimerCounter = 0;
}

static void initTIMER2(unsigned int time)
{
    PR2 = 5*time;
    T2CON = 0x0000;
    T2CONbits.TCKPS = TIMER_PRESCALE_1_8;    
    T2CONbits.TON = 1;      /* enable timer */
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 1;
}

inline static void ReleaseTimer2(void)
{
    T2CONbits.TON = 0;
    TimerArray[1].status = TIMER_DISABLE;
    TimerArray[1].TimerCounter = 0;
}

static void initTIMER3(unsigned int time)
{
    PR3 = 5*time;
    T3CON = 0x0000;
    T3CONbits.TCKPS = TIMER_PRESCALE_1_8;
    T3CONbits.TON = 1;
    IFS0bits.T3IF = 0;
    IEC0bits.T3IE = 1;
}

inline static void ReleaseTimer3(void)
{
    T3CONbits.TON = 0;
    TimerArray[2].status = TIMER_DISABLE;
    TimerArray[2].TimerCounter = 0;
}

static void initTIMER4(unsigned int time)
{
    PR4 = 5*time;
    T4CON = 0x0000;
    T4CONbits.TCKPS = TIMER_PRESCALE_1_8;
    T4CONbits.TON = 1;
    IFS1bits.T4IF = 0;
    IEC1bits.T4IE = 1;
}

inline static void ReleaseTimer4(void)
{
    T4CONbits.TON = 0;
    TimerArray[3].status = TIMER_DISABLE;
    TimerArray[3].TimerCounter = 0;
}

static void initTIMER5(unsigned int time)
{
    PR5 = 5*time;
    T5CON = 0x0000;
    T5CONbits.TCKPS = TIMER_PRESCALE_1_8;
    IFS1bits.T5IF = 0;
    IEC1bits.T5IE = 1;
    T5CONbits.TON = 1;
}

inline static void ReleaseTimer5(void)
{
    T5CONbits.TON = 0;
    TimerArray[4].status = TIMER_DISABLE;
    TimerArray[4].TimerCounter = 0;
}

static void initTIMER6(unsigned int time)
{
    PR6 = 5*time;
    T6CON = 0x0000;
    T6CONbits.TCKPS = TIMER_PRESCALE_1_8;
    IFS2bits.T6IF = 0;
    IEC2bits.T6IE = 1;
    T6CONbits.TON = 1;
}

inline static void ReleaseTimer6(void)
{
    T6CONbits.TON = 0;
    TimerArray[5].status = TIMER_DISABLE;
    TimerArray[5].TimerCounter = 0;
}

void __attribute__((interrupt, auto_psv)) _T1Interrupt(void)
{
    IFS0bits.T1IF = 0;
    TimerArray[0].TimerCounter++;
}

void __attribute__((interrupt, auto_psv)) _T2Interrupt(void)
{
    IFS0bits.T2IF = 0;
    TimerArray[1].TimerCounter++;
}

void __attribute__((interrupt, auto_psv)) _T3Interrupt(void)
{
    IFS1bits.T5IF = 0;
    TimerArray[2].TimerCounter++;
}

void __attribute__((interrupt, auto_psv)) _T4Interrupt(void)
{
    IFS1bits.T5IF = 0;
    TimerArray[3].TimerCounter++;
}

void __attribute__((interrupt, auto_psv)) _T5Interrupt(void)
{
    IFS1bits.T5IF = 0;
    TimerArray[4].TimerCounter++;
}

void __attribute__((interrupt, auto_psv)) _T6Interrupt(void)
{
    IFS2bits.T6IF = 0;
    TimerArray[5].TimerCounter++;
}