#ifndef _MAIN_H
#define	_MAIN_H

#define		CPU_FCY	40 				// for LCD elay referewnce
#ifndef Fcy
#define Fcy     CPU_FCY*1000000
#endif
#define Delay_200uS_Cnt  (Fcy * 0.02) / 1080

#define Setting_mode 1
#define Running_mode 0

typedef char int8_t;
typedef unsigned char   uint8_t;
typedef short  int16_t;
typedef unsigned short  uint16_t;

typedef struct{
    unsigned char buf[256];
    unsigned char counter;
}NormalArray_t;

#endif	/* _MAIN_H */