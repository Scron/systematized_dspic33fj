#ifndef _UART_H
#define _UART_H

#include <xc.h>

#define UART_RX_BUFFER_SIZE             256
#define UART_TX_BUFFER_SIZE             256

#ifndef FCY
#define FCY 40000000
#endif

#define UART_LOW_SPEED_MODE         0
#if (UART_LOW_SPEED_MODE==1)
#else
#define BAUDRATE_DIV            16
#endif

#define BRGVAL_9600             ((FCY/9600)/BAUDRATE_DIV)-1         /* 259 */
#define BRGVAL_19200            ((FCY/19200)/BAUDRATE_DIV)-1
#define BRGVAL_38400            ((FCY/38400)/BAUDRATE_DIV)-1
#define BRGVAL_57600            ((FCY/57600)/BAUDRATE_DIV)-1
#define BRGVAL_115200           ((FCY/115200)/BAUDRATE_DIV)-1
#define SPECIAL_BAUDRATE        14400               /* if not on the list */
#define BRGVAL_SPECIAL          ((FCY/SPECIAL_BAUDRATE)/BAUDRATE_DIV)-1

/* Here we defined the mean that UART function return value */
#define UART_SUCCESS             0x00
#define UART_ERR_PARAMETER       0xFF
#define UART_ERR_BUSY            0xFE
#define UART_NONE_NEW_DATA       0xFD
#define UART_NOW_RECIEVING       0xFC

/* UxMode PDSEL */
enum {
    NONE_PARITY_8BITS = 0,
    EVEN_PARITY_8BITS,
    ODD_PARITY_8BITS,
    NONE_PARITY_9BITS
};

typedef struct {
    char stringflag;
    char messageflag;
    char GetNewData;
    unsigned char msglength;
    unsigned char tx_counter;
    unsigned char rx_counter;
    unsigned char rx_read_counter;
    char tx_buf[UART_TX_BUFFER_SIZE];
    char rx_buf[UART_RX_BUFFER_SIZE];
}UartStructure;

typedef struct {
    void (*func_init)(unsigned int);
    char (*func_writeUartByte)(char);
    char (*func_writeUartString)(char*);
    char (*func_writeUartMsg)(char*,unsigned char);
    unsigned char (*func_readUartBuf)(char*);
    char (*func_getNewdata)(void);
}UartFuncStructure;

typedef void (*Modbus_init_t)(unsigned int);
typedef char (*Modbus_writeMsg_t)(char*,unsigned char);
typedef unsigned char (*Modbus_readRx_t)(char*);
typedef char (*Modbus_getNewdata_t)(void);

//void detectUnexpectData(UartFuncStructure object,unsigned char buf);
void initUART1(unsigned int baudrate);
void initUART2(unsigned int baudrate);
char writeUART1Byte(char data);
char writeUART2Byte(char data);
char writeUART1String(char *data);
char writeUART2String(char *data);
char writeUART1Msg(char *data, unsigned char length);
char writeUART2Msg(char *data, unsigned char length);
unsigned char readUART1buf(char *buf);
unsigned char readUART2buf(char *buf);
char UART1_getNewdata(void);
char UART2_getNewdata(void);

extern UartFuncStructure Uart1FuncStructure,Uart2FuncStructure;

#endif /*_UART_H */
