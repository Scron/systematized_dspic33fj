#ifndef MODBUS_H
#define	MODBUS_H

#include "main.h"


#define MB_BAUD_38400                    38400
#define MB_IDLE                          0x00
#define MB_TRANSMITFUNCTION              0x01
#define MB_WAITING_RESPONSE              0x02
#define MB_GET_PACKAGE                   0x03

#define MB_READ_COILS                    0x01  ///< Modbus function 0x01 Read Coils
#define MB_READ_DISCRETE_INPUTS          0x02  ///< Modbus function 0x02 Read Discrete Inputs
#define MB_WRITE_SINGLE_COIL             0x05  ///< Modbus function 0x05 Write Single Coil
#define MB_WRITE_MULTIPLE_COILS          0x0F  ///< Modbus function 0x0F Write Multiple Coils
#define MB_READ_HOLDING_REGISTERS        0x03  ///< Modbus function 0x03 Read Holding Registers
#define MB_READ_INPUT_REGISTERS          0x04  ///< Modbus function 0x04 Read Input Registers
#define MB_WRITE_SINGLE_REGISTER         0x06  ///< Modbus function 0x06 Write Single Register
#define MB_WRITE_MULTIPLE_REGISTERS      0x10  ///< Modbus function 0x10 Write Multiple Registers
#define MB_MASKWRITE_REGISTER            0x16  ///< Modbus function 0x16 Mask Write Register
#define MB_READWRITE_MULTIPLE_REGISTERS  0x17  ///< Modbus function 0x17 Read Write Multiple Registers

/* Modbus protocol illegal data value exception.`*/
#define MB_ILLEGAL_FUNCTION              0x01
#define MB_ILLEGAL_DATA_ADDRESS          0x02
#define MB_ILLEGAL_DATA_VALUE            0x03
#define MB_ILLEGAL_QTY                   0x04
#define MB_SLAVE_DEVICE_FAILURE          0x05
#define MB_INVAIL_ARGUMENT               0x06
#define MB_SUCCESS                       0x00
#define MB_INVALID_SLAVE_ID              0xE0
#define MB_INVALID_FUNCTION              0xE1
#define MB_RESPONSE_TIMEDOUT             0xE2
#define MB_INVALID_CRC                   0xE3
#define MB_FAILURE                       0xFF

#define MB_MAX_BUFFER_SIZE               256
#define VOID_DATA_ARRAY                  0xFE

typedef struct {
    uint8_t  FunctionCode;
    uint8_t  SlaveID;
    uint16_t Address;
    uint8_t Qty;
    int8_t   *buf;
}MB_struct_t;

void MB_init(UartFuncStructure object,unsigned int baud);
void MB_task(void);
uint8_t MB_SetRequirePackage(   uint8_t  SlaveID,
                                uint8_t  FunctionCode,
                                uint16_t Address,
                                uint8_t Qty,
                                int8_t  *buf);
inline uint8_t CheckModbusStatus(void);

#endif	/* MODBUS_H */

