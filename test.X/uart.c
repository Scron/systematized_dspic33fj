#include <xc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <p33FJ256MC710.h>
#include "uart.h"
#include "main.h"

UartFuncStructure Uart1FuncStructure = {
                      .func_init            = initUART1,
                      .func_writeUartByte   = writeUART1Byte,
                      .func_writeUartString = writeUART1String,
                      .func_writeUartMsg    = writeUART1Msg,
                      .func_readUartBuf     = readUART1buf,
                      .func_getNewdata      = UART1_getNewdata
                  }, 
                  Uart2FuncStructure = {
                     .func_init             = initUART2,
                     .func_writeUartByte    = writeUART2Byte,
                     .func_writeUartString  = writeUART2String,
                     .func_writeUartMsg     = writeUART2Msg,
                     .func_readUartBuf      = readUART2buf,
                     .func_getNewdata       = UART2_getNewdata
                  };

static UartStructure _Uart1stucture,_Uart2stucture;

static void _initUARTstucture(UartStructure object)
{
    object.messageflag      = 0;
    object.stringflag       = 0;
    object.msglength        = 0;
    object.rx_counter       = 0;
    object.tx_counter       = 0;
    object.GetNewData       = 0;
    memset(object.rx_buf,0,UART_RX_BUFFER_SIZE);
    memset(object.tx_buf,0,UART_TX_BUFFER_SIZE);   
}

//void detectUnexpectData(UartFuncStructure object,unsigned char buf)
//{
//    while(object.func_getNewdata() == 1){
//        object.func_readUartBuf(buf,1);
//    }
//}

void initUART1(unsigned int baudrate)
{
    _initUARTstucture(_Uart1stucture);
    
    U1MODE = 0x0000; /* clear all setting */
    U1MODEbits.RTSMD = 1; //1 = UxRTS pin in Simplex mode; 0 = UxRTS pin in Flow Control mode
    U1MODEbits.PDSEL = NONE_PARITY_8BITS; //8-bit data,no parity
    U1MODEbits.STSEL = 0; //1 = Two Stop bits; 0 = One Stop bit

    U1STAbits.UTXISEL0 = 0; // Interrupt generated when any character is transferred to the Transmit Shift register
    U1STAbits.UTXISEL1 = 0;
    U1STAbits.URXISEL = 0; // Interrupt is set when any character is received and transferred from the UxRSR to the receive

    U1BRG = ((FCY / baudrate) / BAUDRATE_DIV) - 1;

    TRISFbits.TRISF2 = 1; //U1 RX
    TRISFbits.TRISF3 = 0; //U1 TX

    IFS0bits.U1TXIF = 0; // Clear the Transmit Interrupt Flag
    IEC0bits.U1TXIE = 0; // Disable Transmit Interrupts
    IFS0bits.U1RXIF = 0; // Clear the Recieve Interrupt Flag
    IEC0bits.U1RXIE = 1; // enable U2RX interrupt

    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART Tx
}

void initUART2(unsigned int baudrate)
{
    _initUARTstucture(_Uart2stucture);
    
    U2MODE = 0x0000; /* clear all setting */
    U2MODEbits.RTSMD = 1; //1 = UxRTS pin in Simplex mode; 0 = UxRTS pin in Flow Control mode
    U2MODEbits.PDSEL = NONE_PARITY_8BITS; //8-bit data,no parity
    U2MODEbits.STSEL = 0; //1 = Two Stop bits; 0 = One Stop bit

    U2STAbits.UTXISEL0 = 0; // Interrupt generated when any character is transferred to the Transmit Shift register
    U2STAbits.UTXISEL1 = 0;
    U2STAbits.URXISEL = 0; // Interrupt is set when any character is received and transferred from the UxRSR to the receive

    U2BRG = ((FCY / baudrate) / BAUDRATE_DIV) - 1;

    TRISFbits.TRISF4 = 1; //U2 RX
    TRISFbits.TRISF5 = 0; //U2 TX

    IFS1bits.U2TXIF = 0; // Clear the Transmit Interrupt Flag
    IEC1bits.U2TXIE = 0; // Disable Transmit Interrupts
    IFS1bits.U2RXIF = 0; // Clear the Recieve Interrupt Flag
    IEC1bits.U2RXIE = 1; // enable U2RX interrupt

    U2MODEbits.UARTEN = 1; // Enable UART
    U2STAbits.UTXEN = 1; // Enable UART Tx
}

char writeUART1Byte(char data)
{
    if (_Uart1stucture.stringflag == 1 || _Uart1stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }
    
    U1TXREG = data;
    return UART_SUCCESS;
}

char writeUART2Byte(char data)
{
    if (_Uart2stucture.stringflag == 1 || _Uart2stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }

    U2TXREG = data;
    return UART_SUCCESS;
}

char writeUART1String(char *data)
{
    unsigned char count = 0;

    if (data == NULL) {
        return UART_ERR_PARAMETER;
    }
    if (_Uart1stucture.stringflag == 1 || _Uart1stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }
    while (data[count++] != '\0');
    memset(_Uart1stucture.tx_buf,0,UART_TX_BUFFER_SIZE);
    memcpy(_Uart1stucture.tx_buf, data, count-1);
    _Uart1stucture.stringflag = 1;
    _Uart1stucture.tx_counter = 0;
    U1TXREG = _Uart1stucture.tx_buf[_Uart1stucture.tx_counter++];
    IEC0bits.U1TXIE = 1;
    return UART_SUCCESS;
}

char writeUART2String(char *data)
{
    int count = 0;

    if (data == NULL) {
        return UART_ERR_PARAMETER;
    }
    if (_Uart2stucture.stringflag == 1 || _Uart2stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }
    while (data[count++] != '\0');
    memset(_Uart2stucture.tx_buf,0,UART_TX_BUFFER_SIZE);
    memcpy(_Uart2stucture.tx_buf, data, count-1);
    _Uart2stucture.stringflag = 1;
    _Uart2stucture.tx_counter = 0;
    U2TXREG = _Uart2stucture.tx_buf[_Uart2stucture.tx_counter++];
    IEC1bits.U2TXIE = 1;
    return UART_SUCCESS;
}

char writeUART1Msg(char *data, unsigned char length)
{
    if (data == NULL) {
        return UART_ERR_PARAMETER;
    }
    if (_Uart1stucture.stringflag == 1 || _Uart1stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }

    memset(_Uart1stucture.tx_buf,0,UART_TX_BUFFER_SIZE);
    memcpy(_Uart1stucture.tx_buf, data, length);
    _Uart1stucture.messageflag = 1;
    _Uart1stucture.msglength = length;
    _Uart1stucture.tx_counter = 0;
    U1TXREG = _Uart1stucture.tx_buf[_Uart1stucture.tx_counter++];
    IEC0bits.U1TXIE = 1;
    return UART_SUCCESS;
}

char writeUART2Msg(char *data, unsigned char length)
{
    if (data == NULL) {
        return UART_ERR_PARAMETER;
    }
    if (_Uart2stucture.stringflag == 1 || _Uart2stucture.messageflag == 1) {
        return UART_ERR_BUSY;
    }

    memset(_Uart2stucture.tx_buf,0,UART_TX_BUFFER_SIZE);
    memcpy(_Uart2stucture.tx_buf, data, length);
    _Uart2stucture.messageflag = 1;
    _Uart2stucture.msglength = length;
    _Uart2stucture.tx_counter = 0;
    U2TXREG = _Uart2stucture.tx_buf[_Uart2stucture.tx_counter++];
    IEC1bits.U2TXIE = 1;
    return UART_SUCCESS;
}

inline char UART1_getNewdata(void)
{
    return _Uart1stucture.GetNewData;
}

inline char UART2_getNewdata(void)
{
    return _Uart2stucture.GetNewData;
}

unsigned char readUART1buf(char *buf,unsigned char Qty)
{
    unsigned char i = 0;
    if(U1STAbits.RIDLE == 0){
        return UART_NOW_RECIEVING;
    }
    if(_Uart1stucture.GetNewData == 0){
        return UART_NONE_NEW_DATA;
    }
    while(i < Qty){ 
        buf[i] = _Uart1stucture.tx_buf[_Uart1stucture.rx_read_counter++];
        if(_Uart1stucture.rx_read_counter == _Uart1stucture.rx_counter){
            _Uart1stucture.GetNewData = 0;
            return UART_SUCCESS;
        }
        i++;
    }
    return UART_SUCCESS;
}

unsigned char readUART2buf(char *buf,unsigned char Qty)
{
    unsigned char i = 0;
    
    if(U2STAbits.RIDLE == 0){
        return UART_NOW_RECIEVING;
    }
    if(_Uart2stucture.GetNewData == 0){
        return UART_NONE_NEW_DATA;
    }
    while(i < Qty){ 
        buf[i] = _Uart2stucture.tx_buf[_Uart2stucture.rx_read_counter++];
        if(_Uart2stucture.rx_read_counter == _Uart2stucture.rx_counter){
            _Uart2stucture.GetNewData = 0;
            return UART_SUCCESS;
        }
        i++;
    }
    return UART_SUCCESS;
}

void __attribute__((interrupt, auto_psv)) _U1TXInterrupt(void)
{
    IFS0bits.U1TXIF = 0;

    if(_Uart1stucture.messageflag == 0 && _Uart1stucture.stringflag == 0){
        IEC0bits.U1TXIE = 0;
        return;
    }
    
    U1TXREG = _Uart1stucture.tx_buf[_Uart1stucture.tx_counter++];

    if (_Uart1stucture.messageflag == 1) {
        if (_Uart1stucture.tx_counter > _Uart1stucture.msglength) {
            _Uart1stucture.tx_counter = 0;
            _Uart1stucture.msglength = 0;
            _Uart1stucture.messageflag = 0;
            IEC0bits.U1TXIE = 0;
        }
    }
    if (_Uart1stucture.stringflag == 1) {
        if (_Uart1stucture.tx_buf[_Uart1stucture.tx_counter] == '\0') {
            _Uart1stucture.tx_counter = 0;
            _Uart1stucture.stringflag = 0;
            IEC0bits.U1TXIE = 0;
        }
    }
}

void __attribute__((interrupt, auto_psv)) _U2TXInterrupt(void)
{
    IFS1bits.U2TXIF = 0;

    if(_Uart2stucture.messageflag == 0 && _Uart2stucture.stringflag == 0){
        IEC1bits.U2TXIE = 0;
        return;
    }
    
    U2TXREG = _Uart2stucture.tx_buf[_Uart2stucture.tx_counter++];

    if (_Uart2stucture.messageflag == 1) {
        if (_Uart2stucture.tx_counter > _Uart2stucture.msglength) {
            _Uart2stucture.tx_counter = 0;
            _Uart2stucture.msglength = 0;
            _Uart2stucture.messageflag = 0;
            IEC1bits.U2TXIE = 0;
        }
    }
    if (_Uart2stucture.stringflag == 1) {
        if (_Uart2stucture.tx_buf[_Uart2stucture.tx_counter] == '\0') {
            _Uart2stucture.tx_counter = 0;
            _Uart2stucture.stringflag = 0;
            IEC1bits.U2TXIE = 0;
        }
    }
}

void __attribute__((interrupt, auto_psv)) _U1RXInterrupt(void)
{
    IFS0bits.U1RXIF = 0;
    _Uart1stucture.GetNewData = 1;
    _Uart1stucture.rx_buf[_Uart1stucture.rx_counter++] = U1RXREG;
    /*
     * Because the rx_counter is defined as unsigned char type, 
     * and we define the UART_RX_BUFFER_SIZE is 256, that would 
     * be fine if rx_counter is overflow.
     */
}

void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void)
{
    IFS1bits.U2RXIF = 0;
    _Uart2stucture.GetNewData = 1;
    _Uart2stucture.rx_buf[_Uart2stucture.rx_counter++] = U2RXREG;
    /*
     * Because the rx_counter is defined as unsigned char type, 
     * and we define the UART_RX_BUFFER_SIZE is 256, that would 
     * be fine if rx_counter is overflow.
     */
}