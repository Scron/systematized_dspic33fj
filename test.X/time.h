#ifndef _TIME_H
#define	_TIME_H

#define TIMER_NUM           6
#define TIMER_DISABLE       0
#define TIMER_ENABLE        1
#define TIMER_PRESCALE_1_1       0b00
#define TIMER_PRESCALE_1_8       0b01
#define TIMER_PRESCALE_1_64      0b10
#define TIMER_PRESCALE_1_256     0b11

#define SUCCES                  0x00
#define INVAID_ARGUMENT         0xFF
#define NONE_AVAILABLE_TIMER    0xFE
#define TIMER_UPPER_LIMIT       13107       // 65535/5 = 13107

typedef void (*TimerFuncptr)(unsigned int);
typedef void (*ReleaseTimerptr)(void);
typedef struct TimerIndex{
    TimerFuncptr    TimerFunc_ptr;
    ReleaseTimerptr ReleaseTimer_ptr;
    int             status;
    int             wait_us;
    unsigned long   TimerCounter;
}TimerIndex_def;
unsigned char ReqAvailableTimer(void);
unsigned char SetTimer_us(unsigned char object,unsigned int time_us);
unsigned char ReleaseTimer(unsigned char object);
inline unsigned long CheckTimerCounter(unsigned char object);
inline void ResetTimerCounter(unsigned char object);

#endif	/* _TIME_H */

